//
//  EMQRecord.swift
//  EMQAssigment
//
//  Created by FDT14009Mac on 2016/4/16.
//  Copyright © 2016年 GoodArc. All rights reserved.
//

import UIKit
import JSONModel

class Source: JSONModel {
    var note:String!
    var sender:String!
}
class Destination: JSONModel {
    var amount:NSNumber!
    var currency:String!
    var recipient:String!
}

class EMQRecord: JSONModel {
    var created:String!
    var destination:Destination!
    var id:NSNumber!
    var source:Source!
}
