//
//  ViewController.swift
//  EMQAssigment
//
//  Created by FDT14009Mac on 2016/4/16.
//  Copyright © 2016年 GoodArc. All rights reserved.
//

import UIKit
import CoreData
import MagicalRecord
import ISO8601
import MBProgressHUD

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate {

    @IBOutlet weak var tableView: UITableView!

    var fetchResultController:NSFetchedResultsController!
    var privateContext:NSManagedObjectContext = NSManagedObjectContext.MR_newPrivateQueueContext()
    var records = [NSManagedObject]()
    var startIndex:Int = 0
    let number = 35
    let estimatedRowHeight:CGFloat = 160.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // clean data
        Record.MR_truncateAll()
        
        func configureFetchResultController() {
            self.privateContext.parentContext = NSManagedObjectContext.MR_defaultContext()
            self.fetchResultController = Record.MR_fetchAllSortedBy("id", ascending: true, withPredicate: nil, groupBy: nil, delegate: self, inContext: self.privateContext)
            self.fetchResultController.fetchRequest.fetchBatchSize = 20
            
        }
        configureFetchResultController()
        self.updateFetchResult()
        
        func configureTableView() {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = estimatedRowHeight
        }
        configureTableView()
        self.loadingMoreData()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableViewDataSource
    func tableView(tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        return self.records.count
    }
    
    func tableView(tableView: UITableView,
                   cellForRowAtIndexPath
        indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:TableViewCell =
            tableView.dequeueReusableCellWithIdentifier("Cell") as! TableViewCell
        
        
        func setValueForCell(){
            let record:Record = records[indexPath.row] as! Record
            cell.amountLabel.text = record.amount?.stringValue
            cell.idLabel.text = record.id?.stringValue
            cell.noteLabel.text = record.note
            cell.createdLabel.text = record.created?.ISO8601String()
            cell.currencyLabel.text = record.currency
            cell.recipientLabel.text = record.recipient
            cell.senderLabel.text = record.sender
        }
        setValueForCell()
        
        func loadingMoreIfNeed() {
            if indexPath.row == records.count - 25 {
                startIndex = records.count
                self.loadingMoreData()
            }
        }
        loadingMoreIfNeed()
        
        return cell
    }
    func tableView(tableView: UITableView,
                   estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return estimatedRowHeight
    }
    
    
    // MARK: Private method
    private func loadingMoreData() -> Void {
        let webMgr:WebMgr = WebMgr()
        webMgr.doRequestInfiniteList(String(startIndex),number:String(number) ) { (responseArray) in
            self.privateContext.performBlock({
                for response in responseArray{
                    let record:EMQRecord = response as! EMQRecord as EMQRecord!
                    let newRecord:Record = Record.MR_findFirstOrCreateByAttribute("id", withValue: record.id, inContext: self.privateContext)
                    newRecord.amount = NSDecimalNumber(string: record.destination.amount.stringValue)
                    newRecord.created = NSDate.init(ISO8601String: record.created)
                    newRecord.currency = record.destination.currency
                    newRecord.note = record.source.note
                    newRecord.sender = record.source.sender
                    newRecord.id = record.id
                }
                self.updateFetchResult()
                dispatch_async(dispatch_get_main_queue(), {
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    self.tableView.reloadData()
                })
            })
        }
    }
    
    private func updateFetchResult(){
        do {
            try self.fetchResultController.performFetch()
        } catch {
            print(error)
        }
        self.records = self.fetchResultController.fetchedObjects as! [NSManagedObject]
    }
}



