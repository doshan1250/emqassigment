//
//  WebMgr.swift
//  EMQAssigment
//
//  Created by FDT14009Mac on 2016/4/16.
//  Copyright © 2016年 GoodArc. All rights reserved.
//

import UIKit
import AFNetworking

class WebMgr: NSObject {

    private var array:NSMutableArray = NSMutableArray()
    private var sessionMgr:AFHTTPSessionManager!
    override init() {
        sessionMgr = AFHTTPSessionManager(baseURL: NSURL.init(string: "https://hook.io/syshen/"))
        sessionMgr.requestSerializer = AFJSONRequestSerializer()
        sessionMgr.responseSerializer = AFJSONResponseSerializer()
        sessionMgr.responseSerializer.acceptableContentTypes =
            NSSet(object: "application/javascript") as Set<NSObject>
    }
    
    func doRequestInfiniteList(startIndex:String,
                               number:String,
                               response: ((responseArray:NSMutableArray!) -> Void)!) -> Void {
        
        let parameters:[String:String] = ["startIndex":startIndex,
                                          "num":number]
        
        sessionMgr.GET("infinite-list",
                    parameters: parameters,
                    success: { (task:NSURLSessionDataTask!, obj:AnyObject!) in
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { 
                            let records:Array<AnyObject> = obj as! Array<AnyObject>
                            for record in records {
                                do {
                                    let emqRecord:EMQRecord = try EMQRecord(dictionary:record as! Dictionary)
                                    self.array.addObject(emqRecord)
                                    
                                } catch {
                                    print(error)
                                }
                            }
                            response(responseArray: self.array)
                        })
                        
                        
        }) { (task:NSURLSessionDataTask!, error:NSError!) in
            response(responseArray: NSMutableArray())
        }
    }
}
