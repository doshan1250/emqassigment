//
//  Record+CoreDataProperties.swift
//  EMQAssigment
//
//  Created by FDT14009Mac on 2016/4/16.
//  Copyright © 2016年 GoodArc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Record {

    @NSManaged var amount: NSDecimalNumber?
    @NSManaged var created: NSDate?
    @NSManaged var currency: String?
    @NSManaged var note: String?
    @NSManaged var recipient: String?
    @NSManaged var sender: String?
    @NSManaged var id: NSNumber?

}
